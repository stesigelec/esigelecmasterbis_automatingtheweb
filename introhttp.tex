% ©2014, Sylvain TENIER - Esigelec, Rouen, France
% Licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) 
% https://creativecommons.org/licenses/by-nc-sa/4.0/

\documentclass{beamer}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usetheme{Warsaw}

\title{Spiding the web: beyond the browser}
\author{Sylvain Tenier - TIC department - Esigelec }
\date{february 18th 2014}

\newcommand {\framedgraphic}[3] {
    \begin{frame}{#1}
	\begin{figure}
	\begin{center}
	    \includegraphics[width=\textwidth,height=0.75\textheight,keepaspectratio]{#2}\caption{#3}
	\end{center}
	\end{figure}
    \end{frame}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Plan}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

\begin{document}
\frame{\titlepage} 

\begin{frame}{Topics of the day}
	\tableofcontents
\end{frame}

\section{What is the web?}

\begin{frame}{The web: original work at CERN}
	\begin{itemize}[<+->]
		\item March 1989: Tim Berners-Lee
		\begin{itemize}
			\item Information Management: A Proposal \href{http://www.w3.org/History/1989/proposal.html} {(link)}
			\item Documents must be \emph{accessible} and \emph{linked together}
		\end{itemize}
	\end{itemize}
	\begin{quote}``a "web" of notes with links (like references) between them is far more useful than a fixed hierarchical system''\end{quote}
	\begin{itemize}[<+->]
		\item October 1990 : development starts on ``world wide web'' program (WWW)
		\item May 1991: general release of ``WWW'' on CERN machines
		\item 1993: Mosaic browser
		\begin{itemize}
			\item The web accounts for 0.1\% of Internet traffic
		\end{itemize}
		\item 2013: bots account for 61\% of web traffic\href{http://www.incapsula.com/blog/bot-traffic-report-2013.html} {(link)} 
	\end{itemize}																								
\end{frame}
 
\begin{frame}{URIs : identifying uniquely a resource}
	\begin{itemize}[<+->]
		\item foo://example.com:8042/over/there?name=ferret\#nose
		\begin{itemize}
			\item foo is the \emph{scheme} 
			\item example.com:8042 is the \emph{authority} 
			\item /over/there is the \emph{path} to the resource
			\item ?name=ferret is a \emph{query}
			\item \#nose is a \emph{fragment}
		\end{itemize}
		\item \url{https://tools.ietf.org/html/rfc3986}
	\end{itemize}
\end{frame}	

\begin{frame}{URIs for the web \emph{locate} resources}
	ex: \url{http://www.esigelec.fr/Formations/Master-en-Sciences-Technologie}
	\begin{itemize}
		\item The scheme is http
		\item The authority contains the fully qualified domain name (FQDN)
		\begin{itemize}
			\item A FQDN identifies a host uniquely
			\item \url{https://tools.ietf.org/html/rfc4703}
		\end{itemize}
		\item The path gives direct access to resources (search engines)
	\end{itemize}
\end{frame}

\begin{frame}{URIs for web applications}
	\url{http://name:pass@example.com:80/path/to/page.html?aVar=aValue\#anchor}
	\begin{itemize}
		\item the \emph{authority} integrates authentication credentials
		\item the \emph{query} string allows for server preprocessing
		\item the \emph{fragment} is a specific place inside the resource
	\end{itemize}
\end{frame}

\begin{frame}{What is a web resource?}
\begin{itemize}[<+->]
    \item \url{http://www.esigelec.fr/en/content/download/1397/19438/MST+Information+Systems+2012+2013.pdf}
	\item But is PDF a ``web page''?
    \item What does HTML mean?
\end{itemize} 
\end{frame}

\framedgraphic{The web: user level}{browserView.PNG}{What a web browser displays} 

\framedgraphic{The web: designer level}{htmlCode.PNG}{What a web designer writes} 

\begin{frame}{HTML and CSS: structuring and formatting}
\begin{itemize}[<+->]
	\item First semester memories?
	\begin{itemize}
		\item Web languages (HTML/CSS) structure and format content
		\item And they are both just plain text files
		\item So where do images or animations \emph{actually} come from?
	\end{itemize}
	\item Let's try!
\end{itemize}
\end{frame}

\begin{frame}{Application 1: Esigelec home page}
\begin{enumerate}
	\item Open Firefox or Chrome
	\item Right-click inside the windows and click on "Inspect Element"
	\item Select the ``network tab''
	\item Enter ``www.esigelec.fr'' in the address bar.
\end{enumerate}
What do you see?
\end{frame}

\framedgraphic{A web page : aggregation of multiple resources}{inspectpage.PNG}{Aggregated resources for Esigelec's home page} 

\framedgraphic{The browser's job}{browserjob.pdf}{What the browser does}
 
\begin{frame}{The next level of web understanding}
\begin{enumerate}[<+->]
	\item User provides URIs to browsers 
	\item Designer constructs HTML pages using special tags to embed other resources
	\item Browsers fetches all resources, constructs and displays the page
	\begin{itemize}
		\item How?
		\item And why do we care?
	\end{itemize}
\end{enumerate}
\end{frame}

\begin{frame}{From web designer to web developer}
\begin{enumerate}
	\item What do we see?
	\begin{itemize}\pause
		\item What the web page author wants us to see!
	\end{itemize}
	\item When do we see it?\pause
	\begin{itemize}
		\item When we manually point our browser to the page
	\end{itemize}
	\item What could we do?\pause
	\begin{itemize}
		\item Get what we need, whenever we need it!\pause
	\end{itemize}
	Let's go and see under the hood.
\end{enumerate}
\end{frame}

\section{Speaking HTTP}
\begin{frame}{What is HTTP?}
\begin{itemize}
	\item HyperText Transfer Protocol 
	\item The mechanics behind the World Wide Web
	\begin{enumerate}
	\item Answers to any request for a web resource (document, graphic, animation..)
	\item Responds to clicks on hypertext links and form submissions
	\end{enumerate}
	\item HTTP is ``just'' a simple, text-based question/answer protocol that takes a request from a client, and provides a response from the server.
\end{itemize}
So how does *one* request for a page translate to all resources being localized, fetched and interpreted?
\end{frame}

\framedgraphic{The full page aggregation process}{browserProcess.pdf}{Localizing, fetching and aggregating resources} 

\framedgraphic{HTTP : the web is an Internet \emph{service}}{TCPIP.pdf}{A client/server communication} 

\begin{frame}{HTTP 0.9: request content, get it or not }
ex: www.example.com/resource.html
\begin{enumerate}
	\item A DNS resolution translates www.example.com to 165.12.125.1
	\item client connects to 165.12.125.1
	\item server accepts connection
	\item client asks for resource.html
	\begin{itemize}
		\item \texttt{GET /resource.html}
	\end{itemize}
	\item server sends content or nothing, and closes connection
\end{enumerate}
\end{frame}

\begin{frame}{From HTTP 0.9 to 1.1... what did we get?}
\begin{enumerate}
 \item Identifying clients and servers
 \item Dealing with different media types
 \item Going beyond found/not found
 \item Maintaining state between connections
 \item Sharing an IP address
 \item Dealing with user data
\end{enumerate}
\end{frame}

\begin{frame}{Application 2: HTTP headers}
Load the Esigelec home page and 
\begin{enumerate}
	\item Display the headers for the first request on the ``Inspect Element'' network tab (see Application 1)
	\item Explain the role of each ``request'' header
	\item Explain the role of each ``response'' header
\end{enumerate}
\end{frame}

\section{Beyond browsing with the Go programming language}
\begin{frame}{What do we need?}
Take control: get what we need, eliminate what we don't
	\begin{itemize}
	\item Send HTTP requests automatically
	\item Analyse data from response body
	\item Create custom pages for visualization
	\end{itemize}
\end{frame}

\begin{frame}{Why go?}
\begin{itemize}
	\item Designed at google, to solve large-scale problems
	\begin{itemize}
		\item \url{http://talks.golang.org/2012/splash.slide}
	\end{itemize}
	\item Go is simple, efficient, safe and fast
	\begin{itemize}
		\item \url{http://talks.golang.org/2012/simple.slide}
	\end{itemize}
	\item Used worldwide!
	\begin{itemize}
		\item \url{http://go-lang.cat-v.org/organizations-using-go}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Application 3 : introduce yourself to go}
\begin{enumerate}
	\item Take the go tour
	\begin{itemize}
		\item \url{http://tour.golang.org}
	\end{itemize}
	\item Follow the ``Web application'' tutorial
	\begin{itemize}
		\item \url{http://golang.org/doc/articles/wiki/} 
	\end{itemize}
	\item Setup the environment in your computer
	\begin{itemize}
		\item \url{http://golang.org/doc/install}
		\item \url{http://golang.org/doc/code.html}
	\end{itemize}
\end{enumerate}
\end{frame}

\begin{frame}{Assignments}
	\begin{enumerate}
		\item Make sure go is properly set up
		\item Complete the Web application tutorial
		\item Create a web server that serves your first-semester resume via HTTP (not file://)
	\end{enumerate}
\end{frame}


\end{document}