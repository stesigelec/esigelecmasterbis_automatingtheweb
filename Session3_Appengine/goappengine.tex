% ©2014, Sylvain TENIER - Esigelec, Rouen, France
% Licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
% https://creativecommons.org/licenses/by-nc-sa/4.0/

\documentclass{beamer}

\usepackage[utf8x]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{listings}
\usetheme{Warsaw}

\title{Hosting Go applications with GAE}
\author{Sylvain Tenier - TIC department - Esigelec }
\date{Monday, March 17 }


\newcommand {\framedgraphic}[3] {
    \begin{frame}{#1}
	\begin{figure}
	\begin{center}
	    \includegraphics[width=\textwidth,height=0.75\textheight,keepaspectratio]{#2}\caption{#3}
	\end{center}
	\end{figure}
    \end{frame}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Plan}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}


\begin{document}

\frame{\titlepage}

\begin{frame}{Topics of the day}
	\tableofcontents
\end{frame}

\section{Motivations}

\begin{frame}{Goals of the day}
  \begin{enumerate}[<+->]
    \item Enhance your skills
    \begin{itemize}
      \item Make your web application public
    \end{itemize}
    \item Evaluate your \texttt{willingness} to learn
    \begin{itemize}
      \item What will we do next?
    \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}{Why deploy to a public location?}
  \begin{itemize}
    \item Your own machine is hidden behind a router
    \item A public location provides a public IP address
    \item You can get your own domain and point to it
  \end{itemize}
\end{frame}

\begin{frame}{Deployment solutions}
  \begin{itemize}[<+->]
   \item Set up or rent a dedicated server
   \item Let others do the work and concentrate on the application
   \begin{itemize}
    \item ``Cloud-based'' solutions
   \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
\frametitle{Dedicated server Pros and cons}
\begin{columns}[t]
  \begin{column}{.48\textwidth}
  \structure{Pros}
  \begin{itemize}
    \item Full control
    \item Host anything you want
    \item Cost-effective for multiple solutions
   \end{itemize}
  \end{column}%
  \hfill%
  \begin{column}{.48\textwidth}
  \structure{Cons}
  \begin{itemize}
    \item Skills needed!
    \item Maintenance time and cost
    \item Cost for small projects and tests
   \end{itemize}
   \end{column}
  \end{columns}
\end{frame}

\begin{frame}
\frametitle{Cloud-based Pros and cons}
\begin{columns}[t]
  \begin{column}{.48\textwidth}
  \structure{Pros}
  \begin{itemize}
    \item Scalablility
    \item Usage-based pricing
    \item No maintenance
   \end{itemize}
  \end{column}%
  \hfill%
  \begin{column}{.48\textwidth}
  \structure{Cons}
  \begin{itemize}
    \item No control on life-cycle and security
    \item Vendor lock-in
    \item Confidentiality
   \end{itemize}
   \end{column}%
  \end{columns}
\end{frame}

\framedgraphic{Google Appengine}{appengine.png}{Google offering for web application deployment}

\begin{frame}
\frametitle{Appengine Pros and cons}
\begin{columns}[t]
  \begin{column}{.48\textwidth}
  \structure{Pros}
  \begin{itemize}
    \item Cloud-based
    \item Supports Go, Java, Python, PHP
    \item Easy integration to Google services
   \end{itemize}
  \end{column}%
  \hfill%
  \begin{column}{.48\textwidth}
  \structure{Cons}
  \begin{itemize}
    \item Cloud-based
    \item Limited to a selection of languages
    \item Specific APIs, crippled features
   \end{itemize}
   \end{column}%
  \end{columns}
\end{frame}

\section{Appengine specific go APIs}

\begin{frame}[fragile]
\frametitle{Configuring your routes}
\lstset{
	language=C,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{columns}[t]
  \begin{column}{.48\textwidth}
    \structure{Vanilla Go}
    \begin{lstlisting}
	//routing is defined in main function
	func main() {
	  //homeHandler func will deliver the home page
	  http.HandleFunc("/", helloHandler)
	  http.HandleFunc("/admin", adminHandler)
	  http.ListenAndServe(":8100", nil) //launch server
	}
   \end{lstlisting}
  \end{column}%
  \hfill%
  \begin{column}{.48\textwidth}
    \structure{Appengine}
    \begin{lstlisting}
	//server is already started
	//init defines muxing routes
	func init() {
	  http.HandleFunc("/", helloHandler)
	  http.HandleFunc("/admin", adminHandler)
	}
     \end{lstlisting}
   \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
\frametitle{Dealing with dynamic data }
\lstset{
	language=C,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{columns}[t]
  \begin{column}{.48\textwidth}
    \structure{Sql with Vanilla Go}
    \begin{lstlisting}
	import (
	  "database/sql"
	  _ "github.com/mattn/go-sqlite3")
	 db, err := sql.Open("sqlite3", "./foo.db")
	 ... //deal with error case
	 defer db.Close()
	sql := `INSERT INTO employes VALUES("John","Manager","20111212"`
	_, err = db.Exec(sql)
	if err != nil {
	//deal with error
	return
	}
   \end{lstlisting}
  \end{column}%
  \hfill%
  \begin{column}{.48\textwidth}
    \structure{Appengine ``NoSQL'' datastore}
    \begin{lstlisting}
    c := appengine.NewContext(r)
    e1 := Employee{
        Name:     "John",
        Role:     "Manager",
        HireDate: time.Now(),
    }
    key, err := datastore.Put(c, datastore.NewIncompleteKey(c, "employee", nil), &e1)
    if err != nil {
        //deal with error
        return
    }
     \end{lstlisting}
   \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
\frametitle{Fetching data from other web sites}
\lstset{
	language=C,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{columns}[t]
  \begin{column}{.48\textwidth}
    \structure{Go \texttt{net/http} package}
    \begin{lstlisting}
	import "net/http"
	...
	resp, err := http.Get("http://example.com/")
	//done! deal with response
	if err != nil {
	  http.Error(w, err.Error(), http.StatusInternalServerError)
	  return
	}
	fmt.Fprintf(w, "HTTP GET returned status %v", resp.Status)
   \end{lstlisting}
  \end{column}%
  \hfill%
  \begin{column}{.48\textwidth}
    \structure{Appengine urlfetch}
    \begin{lstlisting}
	import "net/http"
	import "appengine"
	import "appengine/urlfetch"
	...
	c := appengine.NewContext(r)
	client := urlfetch.Client(c)
	resp, err := client.Get("http://www.google.com/")
	...
     \end{lstlisting}
   \end{column}
  \end{columns}
\end{frame}

\section{Getting started}

\begin{frame}{Prerequisites}
  \begin{itemize}
   \item Last week's work on your resume
   \item Python 2.7
   \item Appengine Go SDK for coding
   \item Google account for deployment
   \item Willingness to work
  \end{itemize}
\end{frame}

\begin{frame}{Practical work}
 \begin{enumerate}
  \item Download from ENT
  \item At the end of the session, upload on ENT
  \begin{itemize}
   \item Your source code
   \item The name of your app on appengine
  \end{itemize}
\end{enumerate}
\end{frame}

\end{document}
