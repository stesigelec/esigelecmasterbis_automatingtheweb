% ©2014, Sylvain TENIER - Esigelec, Rouen, France
% Licensed under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
% https://creativecommons.org/licenses/by-nc-sa/4.0/

\documentclass{beamer}

\usepackage[utf8x]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{listings}
\usetheme{Warsaw}

\title{Serving dynamic content with Go}
\author{Sylvain Tenier - TIC department - Esigelec }
\date{\today}


\newcommand {\framedgraphic}[3] {
    \begin{frame}{#1}
	\begin{figure}
	\begin{center}
	    \includegraphics[width=\textwidth,height=0.75\textheight,keepaspectratio]{#2}\caption{#3}
	\end{center}
	\end{figure}
    \end{frame}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Plan}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}


\begin{document}

\frame{\titlepage}

\begin{frame}{Topics of the day}
	\tableofcontents
\end{frame}

\section{Serving assets and dynamic content}

\begin{frame}[fragile]
\frametitle{Running a web server}
\lstset{
	language=C,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{lstlisting}
	package main
	
	import "net/http"

	func main() {
		//homeHandler func will deliver the home page
		http.HandleFunc("/", homeHandler)
	
		//Wait for TCP connections on port 8100
		http.ListenAndServe(":8100", nil)
	}
\end{lstlisting}
\end{frame}

\begin{frame}{What is a web page?}
 \begin{itemize}
  \item First semester memories?\pause
  \item HTML: structure and content
  \item CSS: presentation
  \item javascript: interactions
 \end{itemize}
\end{frame}

\begin{frame}{Static assets vs dynamic content}
 \begin{enumerate}
  \item images, css and js files are \texttt{static assets}
  \item content is delivered \texttt{dynamically}
  \begin{itemize}[<+->]
   \item static pages are hard and expensive to maintain
   \item content can be loaded from a file, a database...
   \item or fetched from external sources using a \texttt{web crawler}
  \end{itemize}
 \end{enumerate}
\end{frame}

\begin{frame}{Dynamic content in Go}
\begin{itemize}
 \item Structure is written in a \texttt{template} file
 \item Content is injected by providing a data \texttt{structure}
 \begin{itemize}
	\item \url{http://golang.org/pkg/text/template/\#Template.ExecuteTemplate}
 \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Writing a template}
\lstset{
	language=C,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{lstlisting}
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{{.Title}}</title>
  </head> 
  <body>
    <p>welcome to our template generated home page</p>
    <footer>{{.FooterText}}</footer> 
  </body>
</html>
\end{lstlisting}
\end{frame}


\begin{frame}[fragile]
\frametitle{Executing a template}
\lstset{
	language=C,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{lstlisting}
type Page struct {
  Title       string
  FooterText  string
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
  var p Page
  p.Title = "(structure generated) home page"
  p.FooterText = "(c)Some rights reserved"
  err := templates.ExecuteTemplate(w, "pageAccueil.tmpl.html", p)
  if err != nil {
  	log.Panicln("home page cannot be displayed")
  }
}
\end{lstlisting}
\end{frame}


\begin{frame}[fragile]
\frametitle{Serving static content}
\lstset{
	language=C,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{lstlisting}
package main

import (
    "log"
    "net/http"
)

func main() {
  http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir("path/to/staticdir"))))
  err := http.ListenAndServe(":8100", nil)
  if err != nil {
    log.Fatal("Server error: ", err)
  }
}
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Enhanced Routing and dispatching with gorilla mux}
\lstset{
	language=C,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{lstlisting}
import (
	"github.com/gorilla/mux"
	"net/http"
)
func main() {
	//create muxer
	r := mux.NewRouter()
	//muxer rules
	r.HandleFunc("/recipes/add/", addRecipeHandler)
	r.HandleFunc("/recipes/view/{recipeId}", displayRecipeHandler)
	r.HandleFunc("/recipes/list", listRecipesHandler)
	r.HandleFunc("/recipes/add/", addRecipeHandler)
	r.HandleFunc("/", homeHandler)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	//start server
	http.ListenAndServe(":8100", r)
}
\end{lstlisting}
\end{frame}

\section{Setting a go web project up}

\begin{frame}{Creating a go project}
\begin{itemize}
	\item Read carefully \url{http://golang.org/doc/code.html} 
	\item Make sure \texttt{\$GOPATH/bin} is added to your system \texttt{PATH}
	\item Create project in src directory
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{A web project hierarchy}
For project ``\texttt{myproject}'' based at \url{$GOPATH/src/myproject}
\begin{itemize}
	\item go files at project root: \url{$GOPATH/src/myproject}
	\item assets in \url{$GOPATH/src/myproject/static/} 
	\item templates in \url{$GOPATH/src/myproject/templates} 
\end{itemize}
\lstset{
	language=bash,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}
\begin{lstlisting}
	$ ls $GOPATH/src/myproject/
	server.go
	templates/
	  page.html.tmpl
	static/
	  css/
	    style.css
	  img/
	    pic.png
	  js/
	    jquery.js
	    myjs.js
\end{lstlisting}
\end{frame}


\begin{frame}[fragile]
\frametitle{Compiling and running}
\lstset{
	language=bash,
	basicstyle=\ttfamily\scriptsize,
	keywordstyle=\color{blue}\ttfamily,
	stringstyle=\color{red}\ttfamily,
	commentstyle=\color{green}\ttfamily,
	breaklines=true
}

\begin{itemize}
	\item Compiling
		\begin{lstlisting}
		$ cd $GOPATH/src/path/to/project/ 
		$ go install
		\end{lstlisting}
	\item Running
	\begin{enumerate}
		\item Make sure \texttt{\$GOPATH/bin} is in system PATH
		\item Run project.exe from project root (if assets are not embedded or given as flag arguments) 
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}{Pratical}
Download from ENT
\end{frame}

\end{document}
